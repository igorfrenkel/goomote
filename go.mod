module gitlab.com/igorfrenkel/goomote

go 1.18

require github.com/gvalkov/golang-evdev v0.0.0-20220815104727-7e27d6ce89b6

require (
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	github.com/stretchr/testify v1.8.0 // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
)
