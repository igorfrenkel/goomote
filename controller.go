package main

import (
	"errors"
	"log"
	"os"
	"strings"
	"time"

	evdev "github.com/gvalkov/golang-evdev"
)

type controller struct {
	dev  *evdev.InputDevice
	abs  *abskey
	keys chan key
	errs chan error
}

type abskey struct {
	xprev bool
	xval  int32
	yprev bool
	yval  int32
}

type key int

const (
	unknown key = iota
	up
	right
	down
	left
	north
	east
	south
	west
	metaleft
	metaright
	selectkey
	start
)

func (a *abskey) event(evt evdev.InputEvent) key {
	if evt.Code == evdev.ABS_X {
		return a.x(evt.Value)
	} else {
		return a.y(evt.Value)
	}
}

func (a *abskey) x(val int32) key {
	if a.xprev {
		a.xprev = false
		if a.xval-val > 0 {
			return right
		} else {
			return left
		}
	} else { // todo: this is actually a keydown event
		a.xprev = true
		a.xval = val
		return unknown
	}
	return unknown
}

func (a *abskey) y(val int32) key {
	if a.yprev {
		a.yprev = false
		if a.yval-val > 0 {
			return down
		} else {
			return up
		}
	} else {
		a.yprev = true
		a.yval = val
		return unknown
	}
	return unknown
}

func other(evt evdev.InputEvent) key {
	if evt.Value == 0 { // key up
		switch evt.Code {
		case evdev.BTN_NORTH:
			return north
		case evdev.BTN_EAST:
			return east
		case evdev.BTN_SOUTH:
			return south
		case evdev.BTN_C:
			return west
		case evdev.BTN_Z:
			return metaright
		case evdev.BTN_WEST:
			return metaleft
		case evdev.BTN_TL2:
			return selectkey
		case evdev.BTN_TR2:
			return start
		}
	}
	return unknown
}

func eventloop(c *controller) {
	for {
		log.Println("eloop: start")
		evts, err := c.dev.Read()
		log.Print("eloop: evts, err", evts, err)
		if err != nil {
			log.Print("controller: device error", err)
			if strings.Contains(err.Error(), "no such device") {
				log.Print("controller: device went away, polling")
				d, err := getdev("/dev/input/event0")
				if err != nil {
					log.Print("eloop: device err after polling for new device - considering permanent - exit", err)
					return
				}
				c.dev = d
			} else {
				log.Print("eloop: dev read error - considering temporary", err)
				c.errs <- err
			}
			c.errs <- err
			continue
		}
		for _, evt := range evts {
			log.Print("eloop: evt.Type ", evt.Type, evdev.EV_ABS, evdev.EV_KEY)
			switch evt.Type {
			case evdev.EV_ABS:
				log.Print("eloop: abs")
				c.keys <- c.abs.event(evt)
			case evdev.EV_KEY:
				log.Print("eloop: keys")
				c.keys <- other(evt)
			default:
				// log.Print("got unknown event type", evt)
				c.keys <- unknown
			}
		}
	}
}

var pollperiod = time.Second

func getdev(path string) (*evdev.InputDevice, error) {
	log.Print("getdev: looking for device")
	for {
		if _, err := os.Stat(path); err == nil {
			log.Print("getdev.poll: device found")
			return evdev.Open(path)
		} else if errors.Is(err, os.ErrNotExist) {
			log.Print("getdev.poll: device not present, polling")
			<-time.After(pollperiod)
		} else {
			log.Print("getdev.poll: an error was encountered", err)
			return nil, err
		}
	}
}

func newControllerClient() (*controller, error) {
	device, err := getdev("/dev/input/event0")
	if err != nil {
		return nil, err
	}

	// todo: determine keys, errs proper buffering
	c := controller{dev: device, abs: new(abskey), keys: make(chan key, 10000), errs: make(chan error, 10000)}
	go eventloop(&c)
	return &c, nil
}
