package main

import (
	"log"

	"gitlab.com/igorfrenkel/goomote/backpressure"
)

type command int

const (
	unsupported command = iota
	volup
	voldown
	next
	prev
)

func cmd(k key) command {
	switch k {
	case up:
		return volup
	case down:
		return voldown
	case right:
		return next
	case left:
		return prev
	}
	return unsupported
}

var cmds []command

func main() {
	ctrlr, err := newControllerClient()
	if err != nil {
		log.Print("couldn't connect to controller ", err)
		return
	}

	api, err := newApiClient()
	if err != nil {
		log.Print("couldn't connect to api ", err)
		return
	}

	for {
		select {
		case k := <-ctrlr.keys:
			c := cmd(k)
			log.Printf("main: received key from controller %v and have cmd %v", k, c)
			if c == unsupported {
				log.Print("main: unsupported cmd")
			} else {
				if backpressure.Cansend() {
					log.Print("main: sending cmd")
					api.send(c)
				} else {
					log.Printf("main: can't send, dropping cmd")
				}
			}
		case e := <-ctrlr.errs:
			log.Print("received err from controller", e)
		}
	}
}
