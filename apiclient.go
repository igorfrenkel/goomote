package main

import (
	"fmt"
	"io"
	"log"
	"net/http"
	"strings"
)

type apiclient struct {
}

func newApiClient() (*apiclient, error) {
	return new(apiclient), nil
}

func (a *apiclient) send(cmd command) error {
	var endpoint string

	switch cmd {
	case volup:
		endpoint = "volup"
	case voldown:
		endpoint = "voldown"
	case next:
		endpoint = "next"
	case prev:
		endpoint = "prev"
	default:
		return fmt.Errorf("api: unsupported cmd %v", cmd)
	}

	p := strings.Join([]string{a.baseurl(), endpoint}, "/")
	log.Print("put at", p)

	client := &http.Client{}
	req, err := http.NewRequest("PUT", p, nil)
	if err != nil {
		log.Printf("apiclient: error creating new request %s\n", err.Error())
		return err
	}
	resp, err := client.Do(req)
	if err != nil {
		log.Printf("apiclient: error from playerapi PUT %s\n", err.Error())
		return err
	}
	defer resp.Body.Close()

	body, err := io.ReadAll(resp.Body)
	if err != nil {
		log.Printf("apiclient: error from playerapi reading response %s\n", err.Error())
		return err
	}
	log.Printf("apiclient: response from playerapi %s\n", body)
	return nil
}

func (a *apiclient) baseurl() string {
	return "http://192.168.0.130:8900"
}
