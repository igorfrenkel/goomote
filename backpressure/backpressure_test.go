package backpressure

import (
	"testing"
	"time"

	"github.com/stretchr/testify/require"
)

func TestBackpressure(t *testing.T) {
	oldnow := now
	defer func() { now = oldnow }()
	oldlast := lastsent
	defer func() { lastsent = oldlast }()
	lastsent = time.Now()
	var nowtime time.Time
	now = func() time.Time {
		return nowtime
	}
	nowtime = lastsent.Add(dursend - time.Millisecond)
	require.Equal(t, false, Cansend())
	nowtime = lastsent.Add(time.Second + time.Millisecond)
	require.Equal(t, true, Cansend())
}

func TestBackpressureInitial(t *testing.T) {
	require.Equal(t, true, Cansend())
}
