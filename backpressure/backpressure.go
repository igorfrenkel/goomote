package backpressure

import (
	"time"
)

var dursend = time.Duration(time.Second)
var lastsent time.Time

func Cansend() bool {
	now := now()
	if now.Sub(lastsent) > dursend {
		lastsent = now
		return true
	}
	return false
}

var now = func() time.Time {
	return time.Now()
}
